#!/usr/bin/python3


"""

"""

import webapp
from urllib.parse import unquote
import shelve
import random


class randomshort (webapp.webApp):
    with shelve.open('data.db', writeback=True) as s:
        dic_urls = s['data']
    form = """
           <h1>Paste the URL to be shortened</h1>
            <form action="/" method="POST">
                <input type="text" name="url" placeholder="Enter the link here">
                <input type="submit" value="Shorten URL">
                <p>Saved URL</p>
            </form>
            <table border="1">
            <tbody>
            <tr>
            <td>URL CORTA</td>
            <td>URL LARGA</td>
            </tr>
            """
    page = """
            <h1>Your URL has been shortened successfully</h1>
            <p>Shortened URL {largeURL} --> <a href="{shortURL}"> {shortURL} </a></p>
            <p>To short again <a href="http://localhost:1234"> click here </a></p>
            <table border="1">
            <tbody>
            <tr>
            <td>URL CORTA</td>
            <td>URL LARGA</td>
            </tr>
            """
    element_table = """
            <tr>
            <td><a href={id}> {id} </a></td>
            <td>{value}</td>
            </tr>
            
            """

    def parse(self, request):
        try:
            method, resource, _ = request.split(' ', 2)
            body = unquote(request.split('\r\n\r\n', 1)[1])
            return method, resource, body
        except ValueError:
            return "","",""

    def process(self, parsedinfo):
        method, resource, body = parsedinfo
        if method == "GET":
            if resource == "/":
                httpCode = "200 OK"
                htmlBody = randomshort.form.format(dic_url=randomshort.dic_urls)
                for element in randomshort.dic_urls:
                    htmlBody += randomshort.element_table.format(self,id=element,value=randomshort.dic_urls[element])

            else:
                if resource in randomshort.dic_urls:
                    httpCode = "302 Found\r\nLocation: " + randomshort.dic_urls[resource]
                    htmlBody = ""
                else:
                    httpCode = "404 Not Found"
                    htmlBody = "Not Found"
            return httpCode, htmlBody
        elif method == "POST":
            print(body)
            large_url = body.split("=")[1]

            begin = large_url.split("://")[0]
            if not (begin == "http" or begin == "https"):
                large_url = "https://" + large_url
            num = str(random.randint(1000000,9999999))
            short_url ="http://localhost:1234/" + num
            httpCode = "302 Found"
            randomshort.dic_urls["/" + num] = large_url
            with shelve.open('data.db', writeback=True) as s:
                s['data']["/" + num] = large_url
                print("llego aqui")
            htmlBody = randomshort.page.format(largeURL = large_url, shortURL=short_url)
            for element in randomshort.dic_urls:
                htmlBody += randomshort.element_table.format(self, id=element, value=randomshort.dic_urls[element])
                print(element, randomshort.dic_urls[element])
            return httpCode, htmlBody



if __name__ == "__main__":
    testWebApp = randomshort("localhost", 1234)
